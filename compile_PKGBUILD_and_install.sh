#!/bin/sh
set -e

mkdir -p build/
rsync -avz --delete --exclude=.git* ./ build/
cd build/
makepkg --syncdeps --install --noconfirm

