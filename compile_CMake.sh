#!/bin/sh
set -e

cd src/
mkdir ../build/
cd ../build/
cmake ../src/
make
