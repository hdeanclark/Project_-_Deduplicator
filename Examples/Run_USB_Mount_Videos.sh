#!/usr/bin/env bash

find /mnt/usb_mount/hal/Videos/ -type f -print0 |\
    deduplicator \
            -b 1000000  `# Only compare the first XXX bytes.` \
            -n          `# Read NULL-terminated strings from stdin.` \
            -o 1.0      `# Keep files which are older.` \
            -t `# Perform a trial run only. Do not remove anything.` \

