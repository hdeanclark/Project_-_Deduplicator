#!/usr/bin/env bash


find /home/hal/Music/ -size +512k -type f -print0 |\
    deduplicator \
            -S 10000    `# Ignore small files.` \
            -b 10000000 `# Only compare the first XXX bytes.` \
            -n          `# Read NULL-terminated strings from stdin.` \
            -i ' '===-1.0   `# Penalize files with spaces in their names. You would probably detox these files.` \
            -t `# Perform a trial run only. Do not remove anything.` \



