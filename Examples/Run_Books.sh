#!/usr/bin/env bash


find /home/hal/books/ -type f -iname '*pdf' -print0 |\
    deduplicator \
            -b 50000000 `# Only compare the first XXX bytes.` \
            -n          `# Read NULL-terminated strings from stdin.` \
            -i ' '===-1.0   `# Penalize files with spaces in their names. You would probably detox these files.` \
            -i 'xdgopenedfile'===-10.0 `# Penalize files with extra-long names.` \
            -i '-1-'===-1.0 \
            -i '-2-'===-1.0 \
            -i '-3-'===-1.0 \
            -i '__'===-3.0 \
            -i '2014'===-1.0 `# Anything with 2014 in name penalized.` \
            -t `# Perform a trial run only. Do not remove anything.` \



