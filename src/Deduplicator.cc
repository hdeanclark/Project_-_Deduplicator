
#include <iostream>
#include <string>
#include <map>
#include <list>
#include <vector>
#include <fstream>
#include <array>
#include <algorithm>
#include <iomanip>
#include <regex>
#include <ctime>

//#include <sys/mman.h> //For mmap().

//#include <experimental/filesystem>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <boost/iostreams/device/mapped_file.hpp>

#include <boost/algorithm/string/join.hpp>

#include <boost/program_options.hpp>

#include <openssl/md5.h>
#include <openssl/sha.h>

struct FileEntity {
    boost::filesystem::path URI;
    uintmax_t Size;
    std::array<uint8_t,MD5_DIGEST_LENGTH> MD5Sum;    
    std::array<uint8_t,SHA_DIGEST_LENGTH> SHA1Sum;    

    uintmax_t Order;  //Used to keep ordering after sorting.

    std::time_t ModTime;

    double Score; //Used for scoring duplicates.


    // ... other attributes, etc..

    FileEntity(boost::filesystem::path inURI) : URI(inURI), Size(0) { }
    FileEntity(boost::filesystem::path inURI, uintmax_t inOrder) : URI(inURI), Size(0), Order(inOrder) { }

    std::string MD5SumAsString(void) const {
        std::stringstream ss;
        for(int i = 0; i < MD5_DIGEST_LENGTH; i++){
            ss << std::setfill('0') << std::setw(2) << std::hex << static_cast<unsigned int>(MD5Sum[i]);
        }
        return ss.str();
    }

    std::string SHA1SumAsString(void) const {
        std::stringstream ss;
        for(int i = 0; i < SHA_DIGEST_LENGTH; i++){
            ss << std::setfill('0') << std::setw(2) << std::hex << static_cast<unsigned int>(SHA1Sum[i]);
        }
        return ss.str();
    }


};


struct RegexScoringRule {
    std::regex Regex_;
    double Score_;

    RegexScoringRule(const std::regex &regex, double score) : Regex_(regex), Score_(score) { };

    double ComputeScore(const FileEntity &in){
        if(std::regex_search( in.URI.native(), this->Regex_ )){
            return this->Score_;
        }else{
            return 0.0;
        }
    }

};

int main(int argc, char **argv){

    uintmax_t NumberOfBytesToConsider = 1'000'000; // 1 MB
    std::list<boost::filesystem::path> SpecifiedURIs;
    uintmax_t SmallestFileSize = 1; // Only consider files with >= bytes than this number.
    std::string RuleScoreDelimiter = "===";
//    double DefaultRuleScore = 1.0;

    std::vector<RegexScoringRule> ScoringRules;
    bool TrialRunOnly = false;

    double OlderModTimeScore = 0.0; //No default penalty or boost.

    boost::program_options::options_description RegularOptions("General Options");
    RegularOptions.add_options()
        ("help,h", 
         "This help message.")
        ("number-of-bytes-to-consider,b",
         boost::program_options::value<uintmax_t>()->default_value(NumberOfBytesToConsider),
         "The maximum number of bytes of each file to consider. Choose something large enough"
         " so that file headers are overcome, but not so large that the whole file is used,"
         " which can be wasteful if not needed.")
        ("smallest-file-size,S",
         boost::program_options::value<uintmax_t>()->default_value(SmallestFileSize),
         "Only consider files with sizes >= this many bytes.")
        ("uri,u", 
         boost::program_options::value<std::vector<std::string>>()
                                   ->multitoken(),
                                   //->default_value(DefaultURIs, DefaultURIsTextual), 
         "A directory or file to include.")
        ("null-terminated-stdin,n",
         "(Default.) Accept null-terminated uris from stdin. Useful for interoperability with other programs.")
        ("read-from-stdin,R",
         "Read uris from stdin, one per line. Useful for interoperability with other programs.")
//        ("rule-score,s",
//         boost::program_options::value<double>()->default_value(DefaultRuleScore),
//         "The real-valued score to apply to the following rule(s). Highest score 'wins' and is retained.")
        ("trial-run-only,t",
         "Perform a dry-run or trial-run. Do not remove anything.")
        ("older-modification-time-score,o",
         boost::program_options::value<double>()->default_value(OlderModTimeScore),
         "Score to apply to files which have an older modification time."
         " File with highest score 'wins' and is retained. Score is applied once for every file the given file is"
         " older than. So every unique comparison within the matching group is performed."
         " For two files, there is one comparison. For three files, three comparisons."
         " For N files, there are N*(N-1)/2 comparisons. It is therefore best not to mix this"
         " rule with other rules unless the number of potential copies is bounded and known, and the relative"
         " rule weights can be chosen appropriately.")
        ("icase-ecmascript-regex-rule,i",
         boost::program_options::value<std::vector<std::string>>()
                                   ->multitoken(),
         "Criteria for a regex-based rule. Followed by '===' and the real-valued score to apply."
         " File with highest score 'wins' and is retained. Operates on full file path. Case-insensitive.");

//    ("help", "produce help message")
//    ("compression", value<string>(), "compression level")
//    ("verbose", value<string>()->default_value("0"), "verbosity level")  <-- what you want, in case not specified at all.
//    ("verbose", value<string>()->implicit_value("0"), "verbosity level") <-- in case specified impartially, without an argument.
//    ("email", value<string>()->multitoken(), "email to send to")

    boost::program_options::positional_options_description PositionalOptions;
    PositionalOptions.add("uri", -1); //Assume all un-marked options are URIs.

    //PositionalOptions.add("icase-ecmascript-regex-rule", 1);


    boost::program_options::variables_map VarMap;
    boost::program_options::store(
            boost::program_options::command_line_parser(argc, argv)
              .options(RegularOptions)
              .positional(PositionalOptions).run(), 
            VarMap);
    boost::program_options::notify(VarMap);

    if(VarMap.count("help")){
        std::cout << RegularOptions << std::endl;
        return 0;
    }

    if(VarMap.count("trial-run-only")){
        TrialRunOnly = true;
    }

    if(VarMap.count("null-terminated-stdin") && VarMap.count("read-from-stdin")){
        std::cerr << "Cannot specify both newline and null-delimited stdin!" << std::endl;
        std::cerr << RegularOptions << std::endl;
        return 1;
    }

    if(VarMap.count("number-of-bytes-to-consider")){
        NumberOfBytesToConsider = VarMap["number-of-bytes-to-consider"].as<uintmax_t>();
    }

    if(VarMap.count("smallest-file-size")){
        SmallestFileSize = VarMap["smallest-file-size"].as<uintmax_t>();
    }

    if(VarMap.count("uri")){
        const auto URIs = VarMap["uri"].as<std::vector<std::string>>();
        for(auto &URI : URIs){
            SpecifiedURIs.push_back(URI);
        }
    }

    if(VarMap.count("read-from-stdin")){
        //Read from stdin pseudo-file. Split on newline token.
        std::string Shuttle;
        while(std::getline(std::cin, Shuttle).good()){
            SpecifiedURIs.push_back(Shuttle);
            Shuttle.clear();
        }
    }

    if(VarMap.count("null-terminated-stdin") || SpecifiedURIs.empty()){
        //Read from stdin pseudo-file. Split on the null-byte token. You should then have a list of URIs.
        std::string Shuttle;
        while(std::getline(std::cin, Shuttle, '\0').good()){
            SpecifiedURIs.push_back(Shuttle);
            Shuttle.clear();
        }
    }

//    if(VarMap.count("rule-score")){
//        RuleScore = VarMap["rule-score"].as<double>();
//    }

    if(VarMap.count("older-modification-time-score")){
        OlderModTimeScore = VarMap["older-modification-time-score"].as<double>();
        std::cout << "The scores of files with older modification times will be adjusted by "
                  << OlderModTimeScore
                  << " for each unique comparison within the duplicate group." << std::endl;
    }

    if(VarMap.count("icase-ecmascript-regex-rule")){
        //Split the string into two: the regex part and the score part.
        const auto Arguments = VarMap["icase-ecmascript-regex-rule"].as<std::vector<std::string>>();
        for(auto &Argument : Arguments){
            std::regex DelimiterRegex(RuleScoreDelimiter);
            std::sregex_token_iterator it(Argument.begin(), Argument.end(), DelimiterRegex, -1);
            std::sregex_token_iterator end;
            std::vector<std::string> Tokens;
            for( ; it != end; ++it) Tokens.push_back(*it);

            if(Tokens.size() != 2){
                std::cerr << "Option '" << Argument << "' was invalid. Cannot continue." << std::endl;
                return 1;
            }

            std::regex TheRegex(Tokens.front(), std::regex::ECMAScript | std::regex::icase);
            ScoringRules.emplace_back(TheRegex, std::stod(Tokens.back()));

            std::cout << "New rule. Score = " << std::stod(Tokens.back()) << " and regex = " << Tokens.front() << std::endl;
        }

    }

    //-------------------------------------------------------------------------


    //Verify each path is a reachable file or directory. Discard those that are not.
    {
        boost::filesystem::path PathShuttle;
        auto it = SpecifiedURIs.begin();
        while(it != SpecifiedURIs.end()){
            bool wasOK = false;
            try{
                PathShuttle = boost::filesystem::canonical(*it);
                wasOK = boost::filesystem::exists(PathShuttle);
            }catch(const boost::filesystem::filesystem_error &){ }

            if(wasOK){
                //std::cout << "Able to resolve " << *it << " to " << PathShuttle << "" << std::endl;
                *it = PathShuttle;
                ++it;
            }else{
                std::cerr << "Unable to resolve " << *it << ". Ignoring it." << std::endl;
                it = SpecifiedURIs.erase(it);
            }
        }
    }


    //Recursively find all files in the remaining specified URIs.
    std::list<FileEntity> EnumeratedFiles;
    {
        uintmax_t Count = 0;
        for(auto it = SpecifiedURIs.begin(); it != SpecifiedURIs.end(); ++it){
            try{
                if(boost::filesystem::is_regular_file(*it)){
                    if(boost::filesystem::hard_link_count(*it) == 1){
                        EnumeratedFiles.emplace_back(boost::filesystem::canonical(*it), Count++);
                    }else{
                        std::cerr << "Encountered a hard-linked file. Refusing to consider it (because it"
                                  << " isn't taking up any extra disk space)." << std::endl;
                    }
                }else if(boost::filesystem::is_directory(*it) && !boost::filesystem::is_symlink(*it)){
                    auto it_orig_next = std::next(it); //Keep the ordering by maintaining an insertion iterator.
                    for(const auto &DirectoryEntry : boost::filesystem::directory_iterator(*it)){
                        SpecifiedURIs.insert(it_orig_next,DirectoryEntry.path());
                    }
                }
            }catch(const boost::filesystem::filesystem_error &){ }
        }
    }


    //Remove URIs which have been specified more than once.
    {
        auto CompareURIs = [](const FileEntity &L, const FileEntity &R) -> bool {
            return (L.URI < R.URI);
        };
        auto EqualURIs = [](const FileEntity &L, const FileEntity &R) -> bool {
            return (L.URI == R.URI);
        };
        auto CompareOrders = [](const FileEntity &L, const FileEntity &R) -> bool {
            return (L.Order < R.Order);
        };
        EnumeratedFiles.sort(CompareURIs);
        EnumeratedFiles.unique(EqualURIs);
        EnumeratedFiles.sort(CompareOrders);
    }
    std::cout << "Number of files found: " << EnumeratedFiles.size() << std::endl;


    //Compute file sizes and mod times and stow them in the FileEntity instances.
    {
        auto it = EnumeratedFiles.begin();
        while(it != EnumeratedFiles.end()){
            bool wasOK = false;
            try{
                it->Size = boost::filesystem::file_size(it->URI);
                it->ModTime = boost::filesystem::last_write_time(it->URI);

                if(it->Size >= SmallestFileSize) wasOK = true;
                //How can we check std::time_t is valid?

            }catch(const boost::filesystem::filesystem_error &){
                std::cerr << "Unable to compute file size for entity " << it->URI 
                          << ". Ignoring it and continuing" << std::endl;
            }
            if(wasOK){
                ++it;
            }else{
                it = EnumeratedFiles.erase(it);
            }
        }
    }


    //Disregard instances with unique file sizes.
    {
        std::map<uintmax_t,uint32_t> FileSizeCount;
        for(const auto &EnumeratedFile : EnumeratedFiles) FileSizeCount[EnumeratedFile.Size] = 0;
        for(const auto &EnumeratedFile : EnumeratedFiles) FileSizeCount[EnumeratedFile.Size] += 1;
    
        auto it = EnumeratedFiles.begin();
        while(it != EnumeratedFiles.end()){
            if(FileSizeCount[it->Size] == 1){
                it = EnumeratedFiles.erase(it);
            }else{
                ++it;
            }
        }
    }
    std::cout << "Number of files with non-unique sizes: " << EnumeratedFiles.size() << std::endl;


    //Compute checksums for each file.
    {
        uintmax_t i = 0;
        auto it = EnumeratedFiles.begin();
        while(it != EnumeratedFiles.end()){
        //for(auto &EnumeratedFile : EnumeratedFiles){
            bool Success = false;
            try{
                boost::iostreams::mapped_file_source MemMap(it->URI.native());
                                     //,boost::iostreams::mapped_file::readonly);
                MD5_CTX MD5Context;
                MD5_Init(&MD5Context);
                SHA_CTX SHA1Context;
                SHA1_Init(&SHA1Context);
    
                //The 'paging' technique. (Is this needed when using mmap?)
                //auto FileSize = MemMap.size(); 
                //const auto AmountPerRead = 1024*5;
                //for(uintmax_t i = 0; i < FileSize; i += AmountPerRead){
                //    
                //}
    
                //The 'short MD5' technique where you only look at part of the file.
                //auto AmountToUse = MemMap.size();
                //AmountToUse = std::min(AmountToUse, static_cast<decltype(AmountToUse)>(4096));
                //MD5_Update(&MD5Context, MemMap.const_data(), AmountToUse);
    
                //The 'let mmap worry about paging' technique.
                const auto FileSize = static_cast<uintmax_t>(MemMap.size());
                const auto AmountToUse = std::min(NumberOfBytesToConsider,FileSize);
                MD5_Update(&MD5Context, MemMap.data(), AmountToUse);
                SHA1_Update(&SHA1Context, MemMap.data(), AmountToUse);
    
                MD5_Final(&(it->MD5Sum[0]), &MD5Context);
                SHA1_Final(&(it->SHA1Sum[0]), &SHA1Context);
                Success = true;
            }catch(const std::exception &e){
                std::cerr << "Unable to memory-map file " << it->URI
                          << " due to '" << e.what() << "'. Skipping file." << std::endl;
            }

            //std::cout << "MD5Sum for " << it->URI << " was " << it->MD5SumAsString() << std::endl;

            //To avoid reading the whole file, suggestion to use something like this:
            //while ((bytes = fread (data, 1, 1024, inFile)) != 0) MD5_Update(&MD5Context, data, bytes);
            // But because it is memory mapped, I wonder if I can just play with the array index?
            if(Success){
                std::cerr << "Hashing: " << i << " / " << EnumeratedFiles.size() 
                          << " = " << 100*i/EnumeratedFiles.size() << " %"
                          << "\t" << it->URI << std::endl; 
                ++i;
                ++it;
            }else{
                it = EnumeratedFiles.erase(it);
            }
        }
    }


    //Disregard instances with unique checksums.
    {
        std::map<decltype(FileEntity::MD5Sum),uint32_t> MD5SumCount;
        std::map<decltype(FileEntity::SHA1Sum),uint32_t> SHA1SumCount;
        for(const auto &EnumeratedFile : EnumeratedFiles){
            MD5SumCount[EnumeratedFile.MD5Sum] = 0;
            SHA1SumCount[EnumeratedFile.SHA1Sum] = 0;
        }
        for(const auto &EnumeratedFile : EnumeratedFiles){
            MD5SumCount[EnumeratedFile.MD5Sum] += 1;
            SHA1SumCount[EnumeratedFile.SHA1Sum] += 1;
        }

        auto it = EnumeratedFiles.begin();
        while(it != EnumeratedFiles.end()){
            if((MD5SumCount[it->MD5Sum] == 1) || (SHA1SumCount[it->SHA1Sum] == 1)){
                it = EnumeratedFiles.erase(it);
            }else{
                ++it;
            }
        }
    }


    //Group the suspected duplicates using a stable sort.
    {
        auto CompareLTSizeMD5SumSHA1Sum = [](const FileEntity &L, const FileEntity &R) -> bool {
            if(L.Size != R.Size){
                return (L.Size < R.Size);
            }else if(L.MD5Sum != R.MD5Sum){
                return (L.MD5Sum < R.MD5Sum);
            }
            return (L.SHA1Sum < R.SHA1Sum);
        };
        EnumeratedFiles.sort(CompareLTSizeMD5SumSHA1Sum);
    }

    //Usher duplicates into their own containers.
    std::list<std::list<FileEntity>> Duplicates;
    {
        auto AreDuplicates = [](const FileEntity &L, const FileEntity &R) -> bool {
            return (L.Size == R.Size) && (L.MD5Sum == R.MD5Sum) && (L.SHA1Sum == R.SHA1Sum);
        };

        //std::list<FileEntity> Shuttle({EnumeratedFiles.front()});
        while(!EnumeratedFiles.empty()){
            auto itA = EnumeratedFiles.begin();
            auto itB = itA;

            //Iterate itB until it finds a non-equal entity or runs out of runway.
            while((itB != EnumeratedFiles.end()) && AreDuplicates(*itA,*itB)){ ++itB; }

            Duplicates.emplace_back();
            Duplicates.back().splice(Duplicates.back().end(), EnumeratedFiles, itA, itB);

            //Disregard any singles, which occur when file sizes are shared but checksums differ.
            if(Duplicates.back().size() < 2) Duplicates.pop_back();
        }
    }


    //Print the duplicate sets.
    if(false){
        for(const auto &DuplicateSet : Duplicates){
            for(const auto &Duplicate : DuplicateSet){
                //std::cout << Duplicate.MD5SumAsString() << "   " 
                //          << Duplicate.SHA1SumAsString() << "   " 
                std::cout << Duplicate.Size << "    " 
                          << Duplicate.URI.native() << std::endl;
            }
            std::cout << std::endl;
        }
    }


    //Score the entities based on user criteria.
    for(auto &DuplicateSet : Duplicates){

         //We use a score-based system. The file with the highest score will be retained.
         // Scores can go positive or negative. The amount is specified by the user.
         // All FileEntities with the same score will be kept. In this case, you will need
         // to provide more specific scoring criteria.

//         std::map< FileEntity *, double > Scores;
//         for(auto &Duplicate : DuplicateSet) Scores[ std::addressof(Duplicate) ] = 0.0;
         for(auto & Duplicate : DuplicateSet) Duplicate.Score = 0.0;

         //Regex-based scoring.
         for(auto & Duplicate : DuplicateSet){
             for(auto & Rule : ScoringRules){
                 Duplicate.Score += Rule.ComputeScore(Duplicate);
             }
         }


         //Modification time scoring.
         if(OlderModTimeScore != 0.0){
             for(auto itA = std::begin(DuplicateSet); itA != std::end(DuplicateSet); ++itA){
                 for(auto itB = std::next(itA); itB != std::end(DuplicateSet); ++itB){
                     if(itA->ModTime < itB->ModTime){
                         itA->Score += OlderModTimeScore;
                     }else if(itA->ModTime > itB->ModTime){
                         itB->Score += OlderModTimeScore;
                     }
                 }
             }
         }
 
         //Directory separator scoring -- the more '/' are present, the higher the score.

         //Other scoring metrics here...

         // ...

    }

    //Count the number of bytes that will be reclaimed.
    if(true){
        uintmax_t TotalSize = 0;
        for(const auto &DuplicateSet : Duplicates){
            if(DuplicateSet.size() < 2) continue; //Probably not needed, but just in case.
            for(const auto &Duplicate : DuplicateSet){
                TotalSize += Duplicate.Size;
            }
            TotalSize -= DuplicateSet.front().Size;
        }
        std::cout << "The amount of reclaimed space will be: "
                  << TotalSize / 1000000 << " MB."
                  << std::endl;
    }


    //For each set of duplicates, figure out which one is the 'true' version to keep.
    // The file with the highest score will be retained.
    {
        auto CompareScoresGT = [](const FileEntity &L, const FileEntity &R) -> bool {
            return (L.Score > R.Score); 
        };  

        for(auto &DuplicateSet : Duplicates){
            //Final verification that there are multiple files in this cohort.
            if(DuplicateSet.size() < 2) continue;

            DuplicateSet.sort(CompareScoresGT);

            std::cout << "List of files in this cohort: " << std::endl;
            for(auto &Duplicate : DuplicateSet){
                std::cout << Duplicate.Score << "    "
                          << Duplicate.URI.native() << std::endl;
            }
            std::cout << std::endl;

            //If the highest-scoring file does not have a unique best score, do not delete anything.
            {
                const auto itA = DuplicateSet.begin();
                const auto itB = std::next(itA);
                if(itA->Score == itB->Score){
                    std::cout << "Not deleting any files from the set: " << std::endl;
                    for(auto &Duplicate : DuplicateSet) std::cout << "\t" <<  Duplicate.URI << std::endl;
                    std::cout << "\tbecause the highest-scoring file did not have a unique top-score." << std::endl;
                    std::cout << std::endl;
                    continue;
                }else{
                    DuplicateSet.pop_front();
                    for(auto &Duplicate : DuplicateSet){
                        std::cout << "Removing file " << Duplicate.URI << " ... ";
                        if(TrialRunOnly){
                            std::cout << " TRIAL RUN ENABLED! " << std::endl;
                        }else{ 
                            try{
                                std::cout << (boost::filesystem::remove(Duplicate.URI) ? "SUCCESS" : "FAILED");
                                std::cout << std::endl;
                            }catch(const std::exception &e){
                                std::cerr << "Unable to remove file " << Duplicate.URI
                                          << " due to '" << e.what() << "'. Skipping file." << std::endl;
                            }
                        }
                    }
                }
            }
            std::cout << std::endl;
        }
    }

    return 0;
}

